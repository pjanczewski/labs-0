﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Zwierze
    {
        public Zwierze() { }

        protected string Wycie = "Wycie zwierza";
        protected string Nazwa = "";


        // aby uniknac zasady że
        // warunki nie mogą być wzmocnione w podtypach
        // tworze metode sprawdzajaca juz w klasie Zwierze
        // metoda sprawdza czy zwierze moze zawyc
        public virtual bool czyMozeZawyc()
        {
            return true;
        }

        // metoda zwraca wycie zwierzecia
        // dodaje warunki w klasie bazowej
        public virtual string Zawyj()
        {
            if (this.czyMozeZawyc())
                return this.Wycie;
            return "";
        }

        public override string ToString()
        {
            return this.Nazwa + " " + this.Zawyj();
        }


    }
}
