﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Wilk : Zwierze
    {
        Boolean noc;

        public Wilk()
        {
            this.Nazwa = "Wilk";
            this.Wycie = "Auuuuuuuuuuuuuu!";
            this.noc = false;
        }


        // zamiast override metody Zawyj() nadpisuje bazowa metode czyMozeZawyc()
        // w ten sposob warunki ustawiamy w tej metodzie nie zmieniajac glownej funkcjonalnosci klasy Zwierze
        public override bool czyMozeZawyc()
        {
            if (this.noc)
            {
                return true;
            }
            return false;
        }
    }
}
